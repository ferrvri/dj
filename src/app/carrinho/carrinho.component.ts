import { Component, OnInit } from '@angular/core';
import { MockService } from '../_extensions/_services/mock.service';
import { CarrinhoService } from '../_extensions/_services/carrinho.service';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.sass']
})
export class CarrinhoComponent implements OnInit {

  carrinho = []

  constructor(
    private _carrinho: CarrinhoService
  ) { }

  ngOnInit(): void {
    this._carrinho.getCarrinho().subscribe(produtos => {
      this.carrinho = produtos
    })
  }

  decrement(produto){
    produto.Quantidade > 0 ? produto.Quantidade -= 1: produto.Quantidade = 0
    this.updateCarrinho(produto)
  }

  increment(produto){
    produto.Quantidade += 1
    this.updateCarrinho(produto)
  }

  updateCarrinho(produto){
    this._carrinho.getCarrinho().subscribe(produtos => {
      let prod = produtos.filter(p => {
        return p.Id == produto.Id
      })[0]

      if (prod !== undefined){
        prod.Quantidade = produto.Quantidade
      }

      produtos.forEach((p) => {
        if (p.Id == prod.Id){
          p = prod
        }
      })

      this._carrinho.setCarrinho(produtos)
    })
  }

}
