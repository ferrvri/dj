import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { CarrinhoComponent } from './carrinho.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../_shared/shared.module';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    { path: '', component: CarrinhoComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        SharedModule
    ],
    exports: [RouterModule],
    declarations: [CarrinhoComponent]
})
export class CarrinhoModule { }