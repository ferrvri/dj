import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { UsuarioModel } from './models/UsuarioModel';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() { }

    getSession(): Observable<UsuarioModel> {
        return new Observable(subscriber => {
            subscriber.next(JSON.parse(localStorage.getItem('session')) || {})
        })
    }

    removeSession(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            localStorage.removeItem('session')
            resolve(!!localStorage.getItem('session'))
        })
    }

    addSession(usuario: UsuarioModel) {
        localStorage.setItem('session', JSON.stringify(usuario))
        return !!localStorage.getItem('session')
    }
}
