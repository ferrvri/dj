import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import produtos from '../_data/produtos.json';
import { ProdutoModel } from './models/ProdutoModel';

@Injectable({
  providedIn: 'root'
})
export class MockService {

  constructor() { }

  getProdutos(): Observable<ProdutoModel[]> {
    return new Observable(subscriber => {
      subscriber.next(produtos)
    })
  }
}
