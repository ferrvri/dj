import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ProdutoModel } from './models/ProdutoModel';

@Injectable({
  providedIn: 'root'
})
export class CarrinhoService {

  constructor() { }

  getCarrinho(): Observable<ProdutoModel[]> {
    return new Observable(subscriber => {
      subscriber.next(JSON.parse(localStorage.getItem('carrinho')) || [])
    })
  }

  addCarrinho(produto: ProdutoModel){
    let carrinho = JSON.parse(localStorage.getItem('carrinho')) || []
    carrinho.push(produto)

    localStorage.setItem('carrinho', JSON.stringify(carrinho))
  }

  setCarrinho(produtos: ProdutoModel[]){
    localStorage.setItem('carrinho', JSON.stringify(produtos))
  }
}
