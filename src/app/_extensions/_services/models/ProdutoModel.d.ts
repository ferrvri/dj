export interface ProdutoModel{
    Id: number;
    Nome: string;
    Preco: number;
    Quantidade: number;
    QuantidadeEstoque: number;
}