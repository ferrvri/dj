import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ProdutoModel } from './models/ProdutoModel';
import { StorageService } from './storage.service';

import sweetAlert from 'sweetalert'

@Injectable({
  providedIn: 'root'
})
export class LoginAlertService {

  constructor(
    private _storage: StorageService
  ) { }

  fire() {
    return new Promise((resolve, reject) => {
      let div = document.createElement('div')
      div.innerHTML = `
        <div class="form-control">
          <label class="label" for="email">E-mail</label>
          <input class="control" type="email" id="email" />
        </div>
        <div class="form-control">
          <label class="label" for="senha">Senha</label>
          <input class="control" type="password" id="senha" />
        </div>
      `;

      return sweetAlert({
        title: 'Faça o login',
        content: (div as any),
        buttons: {
          cancelar: {
            text: 'Cancelar',
            closeModal: true,
            className: 'default'
          },
          entrar: {
            text: 'Entrar',
            value: true
          }
        }
      }).then(data => {
        if (data == true) {
          let email = (div.querySelector('#email') as HTMLInputElement).value
          let senha = (div.querySelector('#senha') as HTMLInputElement).value
          if (email.length < 1 || senha.length < 1) {
            sweetAlert({
              icon: 'error',
              title: 'Preencha os campos'
            })
          } else {
            if (email.split('@').length > 0) {
              this._storage.addSession({ Email: email, Senha: senha })
              resolve(true)
            } else {
              sweetAlert({
                icon: 'error',
                title: 'Preencha com um e-mail'
              })
            }
          }
        }
      })
    })
  }


}
