import { Component, OnInit, Input } from '@angular/core';

import sweetAlert from 'sweetalert';
import { StorageService } from 'src/app/_extensions/_services/storage.service';
import { CarrinhoService } from 'src/app/_extensions/_services/carrinho.service';
import { LoginAlertService } from 'src/app/_extensions/_services/login.alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.sass']
})
export class SideBarComponent implements OnInit {
  _state = false

  get state(): boolean {   
    return this._state
  }

  @Input('state')
  set state(value: boolean) {
    this._state = value
    if (this._state){
      this.openMenu()
    }
  }

  carrinho = []
  carrinhoItems = 0

  usuario = {Email: undefined}

  constructor(
    private _storage: StorageService,
    private _carrinho: CarrinhoService,
    private _loginAlert: LoginAlertService,
    public _router: Router
  ) { }

  ngOnInit(): void {
    this._carrinho.getCarrinho().subscribe(carrinho => {
      carrinho.forEach(c => {
        this.carrinhoItems += c.Quantidade
      })
      this.carrinho = carrinho
    })
    this._storage.getSession().subscribe(usuario => this.usuario = usuario)    
  }

  openMenu() {
    document.getElementById('side').classList.toggle('slide-in');
    document.getElementById('main').classList.toggle('slide-content');
  }

  entrar(){
    this._loginAlert.fire().then(_ => {
      window.location.reload();
    })
  }

  sair(){
    this._storage.removeSession().then(_ => {
      this._carrinho.setCarrinho([])
      window.location.reload()
    })
  }

  navigate(route){
    this._router.navigateByUrl(route)
  }
}
