import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../_shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DetalheComponent } from './detalhe.component';

const routes: Routes = [
    { path: '', component: DetalheComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        SharedModule
    ],
    exports: [RouterModule],
    declarations: [DetalheComponent]
})
export class DetalheModule { }