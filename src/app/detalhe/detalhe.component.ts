import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MockService } from '../_extensions/_services/mock.service';
import { CarrinhoService } from '../_extensions/_services/carrinho.service';
import { StorageService } from '../_extensions/_services/storage.service';
import { LoginAlertService } from '../_extensions/_services/login.alert.service';
import { ProdutoModel } from '../_extensions/_services/models/ProdutoModel';

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.component.html',
  styleUrls: ['./detalhe.component.sass']
})
export class DetalheComponent implements OnInit {

  produto;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _mock: MockService,
    private _carrinho: CarrinhoService,
    private _storage: StorageService,
    private _loginAlert: LoginAlertService
  ) { }

  ngOnInit(): void {
    this._mock.getProdutos().subscribe(prods => {
      this._activatedRoute.params.subscribe(params => {
        this.produto = prods.filter(p => {
          return p.Id == params.id
        })[0]

        this._carrinho.getCarrinho().subscribe(carrinho => {
          let item = carrinho.filter(c => {
            return c.Id == this.produto.Id
          })[0]

          if (item !== undefined) {
            this.produto.Quantidade = item.Quantidade
          }
        })
      })
    })
    console.log(this.produto)
    
  }

  addItem(produto: ProdutoModel){
    this._storage.getSession().subscribe(usuario => {
      if (usuario.Email == undefined){
        this._loginAlert.fire().then(_ => {
          produto.Quantidade += 1
          this._carrinho.addCarrinho(produto)

          window.location.reload()
        })
      }else{
        produto.Quantidade += 1
        this._carrinho.addCarrinho(produto)
      }
    })
  }


  decrement(produto){
    produto.Quantidade > 0 ? produto.Quantidade -= 1: produto.Quantidade = 0
    this.updateCarrinho(produto)
  }

  increment(produto){
    produto.Quantidade += 1
    this.updateCarrinho(produto)
  }

  updateCarrinho(produto){
    this._carrinho.getCarrinho().subscribe(produtos => {
      let prod = produtos.filter(p => {
        return p.Id == produto.Id
      })[0]

      if (prod !== undefined){
        prod.Quantidade = produto.Quantidade
      }

      produtos.forEach((p) => {
        if (p.Id == prod.Id){
          p = prod
        }
      })

      this._carrinho.setCarrinho(produtos)
    })
  }
}
