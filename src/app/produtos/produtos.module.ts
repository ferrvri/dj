import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ProdutosComponent } from './produtos.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../_shared/shared.module';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    { path: '', component: ProdutosComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        SharedModule
    ],
    exports: [RouterModule],
    declarations: [ProdutosComponent]
})
export class ProdutosModule { }