import { Component, OnInit } from '@angular/core';
import { MockService } from '../_extensions/_services/mock.service';
import { finalize, map, windowWhen } from 'rxjs/operators';
import { StorageService } from '../_extensions/_services/storage.service';
import { LoginAlertService } from '../_extensions/_services/login.alert.service';
import { ProdutoModel } from '../_extensions/_services/models/ProdutoModel';
import { CarrinhoService } from '../_extensions/_services/carrinho.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.sass']
})
export class ProdutosComponent implements OnInit {

  produtos = []
  usuario = {}

  constructor(
    private _mock: MockService,
    private _storage: StorageService,
    private _loginAlert: LoginAlertService,
    private _carrinho: CarrinhoService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this._storage.getSession().subscribe(usuario => {
      this.usuario = usuario
    })

    let prods = this._mock.getProdutos().pipe(
      finalize(() => {
        prods.unsubscribe();
      })
    ).subscribe(response => {
      this._carrinho.getCarrinho().subscribe(prod => {
        response.forEach(r => {
          let produto = prod.filter(p => {
            return p.Id == r.Id
          })[0]
          
          if (produto !== undefined){
            r.Quantidade = produto.Quantidade
          }
        })
      })

      this.produtos = response
    })
  }

  addItem(produto: ProdutoModel){
    this._storage.getSession().subscribe(usuario => {
      if (usuario.Email == undefined){
        this._loginAlert.fire().then(_ => {
          produto.Quantidade += 1
          this._carrinho.addCarrinho(produto)

          window.location.reload()
        })
      }else{
        produto.Quantidade += 1
        this._carrinho.addCarrinho(produto)
      }
    })
  }

  decrement(produto){
    produto.Quantidade > 0 ? produto.Quantidade -= 1: produto.Quantidade = 0
    this.updateCarrinho(produto)
  }

  increment(produto){
    produto.Quantidade += 1
    this.updateCarrinho(produto)
  }

  updateCarrinho(produto){
    this._carrinho.getCarrinho().subscribe(produtos => {
      let prod = produtos.filter(p => {
        return p.Id == produto.Id
      })[0]

      if (prod !== undefined){
        prod.Quantidade = produto.Quantidade
      }

      produtos.forEach((p) => {
        if (p.Id == prod.Id){
          p = prod
        }
      })

      this._carrinho.setCarrinho(produtos)
    })
  }

  navigate(route){
    this._router.navigate(route)
  }
}
