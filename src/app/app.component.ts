import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  opened = false

  ngOnInit(){}

  openMenu(event){
    (event.target as HTMLAnchorElement).classList.toggle('menu-clicked')
    this.opened = !this.opened
  }

  entrar(){
    console.log('ok')
  }
}

